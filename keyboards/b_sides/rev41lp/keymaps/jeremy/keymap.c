/*
Copyright 2022 @jcowgar

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include QMK_KEYBOARD_H

#define _COL 0
#define _NAV 1
#define _SYM 2
#define _NUM 3
#define _VIW 4
#define _TMX 5
#define _PRG 6

enum userspace_custom_keycodes {
  PLACEHOLDER = SAFE_RANGE, // Can always be here
  VIW_UP,  // Vim Window Up
  VIW_DN,  // Vim Window Down
  VIW_LF,  // Vim Window Left
  VIW_RG,  // Vim Window Right
  TMX_UP,  // Tmux Window Up
  TMX_DN,  // Tmux Window Down
  TMX_LF,  // Tmux Window Left
  TMX_RG,  // Tmux Window Right
  TMX_PW,  // Tmux Previous Window
  TMX_NW,  // Tmux Next Window
  MC_PEQ,  // +=
  MC_EQEQ, // ==
  MC_MEQ,  // -=
  MC_SEQ,  // *=
  MC_DEQ,  // /=
  MC_CEQ,  // :=
  MC_NEQ,  // !=
  MC_RBE,  // {\n
};


const uint16_t PROGMEM keymaps[][MATRIX_ROWS][MATRIX_COLS] = {
  [_COL] = LAYOUT_rev41lp(
    KC_ESC,  KC_Q,       KC_W, KC_F, KC_P, KC_G,                   KC_J, KC_L, KC_U,    KC_Y,   KC_SCLN,         KC_BSPC,
    KC_LCTL, KC_A,       KC_R, KC_S, KC_T, KC_D,                   KC_H, KC_N, KC_E,    KC_I,   KC_O,            LCTL_T(KC_QUOT),
    KC_LALT, LGUI(KC_Z), KC_X, KC_C, KC_V, KC_B,                   KC_K, KC_M, KC_COMM, KC_DOT, LGUI_T(KC_SLSH), LALT_T(KC_RBRC),
                    KC_LSPO, LT(_NAV, KC_SPACE), LT(_NUM, KC_TAB), LT(_SYM, KC_ENT), KC_RSPC
  ),

  [_NAV] = LAYOUT_rev41lp(
    XXXXXXX, QK_BOOT, XXXXXXX, XXXXXXX, XXXXXXX,  XXXXXXX,           XXXXXXX, KC_HOME, KC_UP,   KC_END,   XXXXXXX, KC_DEL,
    KC_LCTL, KC_LCTL, KC_LALT, KC_LGUI, KC_LSFT,  XXXXXXX,           KC_PGUP, KC_LEFT, KC_DOWN, KC_RIGHT, XXXXXXX, LCTL_T(KC_TILD),
    KC_LALT, KC_LGUI, XXXXXXX, XXXXXXX, MO(_VIW), MO(_TMX),          KC_PGDN, XXXXXXX, XXXXXXX,  XXXXXXX, KC_LGUI, KC_LALT,
                                         _______,  _______, _______, _______, _______
  ),

  [_SYM] = LAYOUT_rev41lp(
    KC_GRV,  KC_BSLS,         KC_PERC, KC_SLSH, KC_ASTR, KC_PLUS,          KC_QUES, KC_AMPR,  KC_PIPE, KC_EXLM, KC_SCLN, KC_DEL,
    KC_LCTL, KC_AT,           KC_COLN, KC_LCBR, KC_RCBR, KC_EQL,           KC_HASH, KC_UNDS,  KC_MINS, KC_TILD, KC_DLR,  KC_LCTL,
    KC_LALT, LGUI_T(KC_CIRC), KC_DLR,  KC_LBRC, KC_RBRC, KC_MINS,          MO(_PRG),MO(_PRG), XXXXXXX, XXXXXXX, KC_LGUI, KC_LALT,
                                                _______, _______, _______, _______,  _______
  ),

  [_NUM] = LAYOUT_rev41lp(
    XXXXXXX, KC_F9,         KC_F10,        KC_F11,        KC_F12,        XXXXXXX,           KC_MINS, KC_7, KC_8, KC_9, KC_SLSH,        XXXXXXX,
    KC_LCTL, LCTL_T(KC_F4), LALT_T(KC_F5), LGUI_T(KC_F6), LSFT_T(KC_F8), XXXXXXX,           KC_PLUS, KC_4, KC_5, KC_6, KC_ASTR,        KC_LCTL,
    KC_LALT, LGUI_T(KC_F1), KC_F2,         KC_F3,         KC_F4,         XXXXXXX,           KC_0,    KC_1, KC_2, KC_3, LGUI_T(KC_EQL), KC_LALT,
                                                                _______, _______, _______,  _______,  _______
  ),

  [_VIW] = LAYOUT_rev41lp(
    _______,   _______,       _______,       _______,       _______,       _______,  /**/   _______, _______, VIW_UP,          _______,        _______,        _______,
    _______,   _______,       _______,       _______,       _______,       _______,  /**/   _______, VIW_LF,  VIW_DN,          VIW_RG,         _______,        _______,
    _______,   _______,       _______,       _______,       _______,       _______,  /**/   _______, _______, _______,         _______,        _______,        _______,
                                                                _______, _______, _______,  _______,  _______
  ),

  [_TMX] = LAYOUT_rev41lp(
    _______,   _______,       _______,       _______,       _______,       _______,  /**/   _______, TMX_PW,  TMX_UP,          TMX_NW,         _______,        _______,
    _______,   _______,       _______,       _______,       _______,       _______,  /**/   _______, TMX_LF,  TMX_DN,          TMX_RG,         _______,        _______,
    _______,   _______,       _______,       _______,       _______,       _______,  /**/   _______, _______, _______,         _______,        _______,        _______,
                                                                _______, _______, _______,  _______,  _______
  ),

  [_PRG] = LAYOUT_rev41lp(
    _______,   _______, _______, MC_DEQ,  MC_SEQ,  MC_PEQ,            _______, _______, _______, MC_NEQ,  _______, _______,
    _______,   _______, MC_CEQ,  MC_RBE,  _______, MC_EQEQ,           _______, _______, _______, _______, _______, _______,
    _______,   _______, _______, _______, _______, MC_MEQ,            _______, _______, _______, _______, _______, _______,
                                          _______, _______, _______,  _______,  _______
  )

  // [_] = LAYOUT_rev41lp(
  //   _______,   _______,       _______,       _______,       _______,       _______,  /**/   _______, _______, _______,         _______,        _______,        _______,
  //   _______,   _______,       _______,       _______,       _______,       _______,  /**/   _______, _______, _______,         _______,        _______,        _______,
  //   _______,   _______,       _______,       _______,       _______,       _______,  /**/   _______, _______, _______,         _______,        _______,        _______,
  //                                                               _______, _______, _______,  _______,  _______
  // )
};

bool process_record_user(uint16_t keycode, keyrecord_t *record) {
    bool pressed = record->event.pressed;

    switch (keycode) {
		case VIW_UP:
			if (pressed) {
				SEND_STRING(SS_LCTL("w") SS_TAP(X_UP));
			}
			return false;
		case VIW_DN:
			if (pressed) {
				SEND_STRING(SS_LCTL("w") SS_TAP(X_DOWN));
			}
			return false;
		case VIW_LF:
			if (pressed) {
				SEND_STRING(SS_LCTL("w") SS_TAP(X_LEFT));
			}
			return false;
		case VIW_RG:
			if (pressed) {
				SEND_STRING(SS_LCTL("w") SS_TAP(X_RIGHT));
			}
			return false;
		case TMX_UP:
			if (pressed) {
				SEND_STRING(SS_LCTL("b") SS_TAP(X_UP));
			}
			return false;
		case TMX_DN:
			if (pressed) {
				SEND_STRING(SS_LCTL("b") SS_TAP(X_DOWN));
			}
			return false;
		case TMX_LF:
			if (pressed) {
				SEND_STRING(SS_LCTL("b") SS_TAP(X_LEFT));
			}
			return false;
		case TMX_RG:
			if (pressed) {
				SEND_STRING(SS_LCTL("b") SS_TAP(X_RIGHT));
			}
			return false;
		case TMX_PW:
			if (pressed) {
				SEND_STRING(SS_LCTL("b") SS_TAP(X_P));
			}
			return false;
		case TMX_NW:
			if (pressed) {
				SEND_STRING(SS_LCTL("b") SS_TAP(X_N));
			}
			return false;
		case MC_DEQ:
			if (pressed) {
				SEND_STRING("/=");
			}
			return false;
		case MC_SEQ:
			if (pressed) {
				SEND_STRING("*=");
			}
			return false;
		case MC_PEQ:
			if (pressed) {
				SEND_STRING("+=");
			}
			return false;
		case MC_EQEQ:
			if (pressed) {
				SEND_STRING("==");
			}
			return false;
		case MC_MEQ:
			if (pressed) {
				SEND_STRING("-=");
			}
			return false;
		case MC_CEQ:
			if (pressed) {
				SEND_STRING(":=");
			}
			return false;
		case MC_NEQ:
			if (pressed) {
				SEND_STRING("!=");
			}
			return false;
		case MC_RBE:
			if (pressed) {
				SEND_STRING("{" SS_TAP(X_ENT));
			}
			return false;
        default:
            return true;
    }
}

