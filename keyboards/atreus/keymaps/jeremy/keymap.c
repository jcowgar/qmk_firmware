// This is the personal keymap of Jeremy Cowgar (@jcowgar). It is written for the programmer.

#include QMK_KEYBOARD_H
#include "action_layer.h"

// Each layer gets a name for readability, which is then used in the keymap matrix below.
#define ALPH 0
#define NUMS 1
#define CURS 2
#define SYMB 3
#define FKEY 4

// Some handy macros to keep the keymaps clean and easier to maintain
#define KM_SAVE LCTL(KC_S)
#define KM_CLSE LCTL(KC_W)
#define KM_OPEN LCTL(KC_O)

#define KM_COPY LCTL(KC_C)
#define KM_CUT  LCTL(KC_X)
#define KM_PAST LCTL(KC_V)
#define KM_UNDO LCTL(KC_Z)
#define KM_REDO LCTL(LSFT(KC_Z))

const uint16_t PROGMEM keymaps[][MATRIX_ROWS][MATRIX_COLS] = {
  [ALPH] = LAYOUT(
    KC_Q,    KC_W,           KC_F,           KC_P,    KC_G,                      KC_J,    KC_L,    KC_U,           KC_Y,           KC_SCLN,
    KC_A,    LT(NUMS, KC_R), LT(FKEY, KC_S), KC_T,    KC_D,                      KC_H,    KC_N,    LT(CURS, KC_E), LT(SYMB, KC_I), KC_O,
    KC_Z,    KC_X,           KC_C,           KC_V,    KC_B,                      KC_K,    KC_M,    KC_COMM,        KC_DOT,         KC_SLSH,
    KC_LCTL, KC_LALT,        KC_LGUI,        KC_LSFT, KC_SPC,  KC_TAB, KC_ESC,   KC_ENT,  KC_RSFT, KC_LGUI,        KC_RALT,        KC_RCTL
  ),
  [NUMS] = LAYOUT(
    KC_TRNS, KC_TRNS,        KC_EXLM,        KC_ASTR, KC_SLSH,                   KC_TRNS, KC_7,    KC_8,           KC_9,           KC_SLSH,
    KC_TRNS, KC_TRNS,        KC_PLUS,        KC_EQL,  KC_MINS,                   KC_LPRN, KC_4,    KC_5,           KC_6,           KC_ASTR,
    KC_TRNS, KC_TRNS,        KC_LABK,        KC_RABK, KC_SCLN,                   KC_RPRN, KC_1,    KC_2,           KC_3,           KC_MINS,
    KC_TRNS, KC_TRNS,        KC_TRNS,        KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_0,    KC_DOT,         KC_EQL,         KC_PLUS
  ),
  [CURS] = LAYOUT(
    KC_TRNS, KC_BSPC,        KC_UP,          KC_DEL,  KC_PGUP,                   KC_TRNS, KM_SAVE, KC_TRNS,        KM_OPEN,        KC_TRNS,
    KC_TRNS, KC_LEFT,        KC_DOWN,        KC_RGHT, KC_PGDN,                   KM_UNDO, KC_LGUI, KC_TRNS,        KC_LALT,        KC_LCTL,
    KC_TRNS, KC_VOLD,        KC_MUTE,        KC_VOLU, KC_MPLY,                   KM_REDO, KM_CLSE, KC_TRNS,        KC_TRNS,        KC_TRNS,
    KC_TRNS, KC_TRNS,        RESET,          KC_TRNS, KC_TAB,  KM_PAST, KM_COPY, KM_CUT,  KC_TRNS, KC_TRNS,        KC_TRNS,        KC_TRNS
  ),
  [SYMB] = LAYOUT(
    KC_BSLS, KC_EXLM,        KC_LABK,        KC_RABK, KC_COLN,                   KC_UNDS, KC_DLR,  KC_QUES,       KC_TRNS,         KC_PERC,
    KC_AT,   KC_AMPR,        KC_LPRN,        KC_RPRN, KC_SCLN,                   KC_GRV,  KC_DQT,  KC_QUOT,       KC_TRNS,         KC_TILD,
    KC_HASH, KC_PIPE,        KC_LCBR,        KC_RCBR, KC_SLSH,                   KC_TRNS, KC_COMM, KC_DOT,        KC_TRNS,         KC_CIRC,
    KC_TRNS, KC_TRNS,        KC_LBRC,        KC_RBRC, KC_UNDS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS,       KC_TRNS,         KC_TRNS
  ),
  [FKEY] = LAYOUT(
    KC_TRNS, KC_TRNS,        KC_TRNS,        KC_TRNS, KC_TRNS,                   KC_TRNS, KC_F9,   KC_F10,        KC_F11,          KC_F12,
    KC_LCTL, KC_LALT,        KC_TRNS,        KC_LGUI, KC_TRNS,                   KC_TRNS, KC_F5,   KC_F6,         KC_F7,           KC_F8,
    KC_TRNS, KC_TRNS,        KC_TRNS,        KC_TRNS, KC_TRNS,                   KC_TRNS, KC_F1,   KC_F2,         KC_F3,           KC_F4,
    KC_TRNS, KC_TRNS,        KC_TRNS,        KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS,       KC_TRNS,         KC_TRNS
  ),
};
