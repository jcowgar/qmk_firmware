#include QMK_KEYBOARD_H

// Navigation on the right hand, numbers on the right hand
// Symbols and function keys on the left
//
// Easy access to the escape key would be nice for vim and escaping completion
   // menus.
//
// Shift access on both hands provides comfort.
//
// Need access to about all keys with any modifier, multiple at the same time.
//
// My thumbs seem to fall on center thumb cluster.
//
// Dynamic macros may be cool to put into the keyboard.
//
// Caps Word seems interesting.
//

#define _COL 0
#define _NAV 1
#define _SYM 2
#define _NUM 3
#define _VIW 4
#define _TMX 5
#define _PRG 6

enum userspace_custom_keycodes {
  PLACEHOLDER = SAFE_RANGE, // Can always be here
  VIW_UP,
  VIW_DN,
  VIW_LF,
  VIW_RG,
  TMX_UP,
  TMX_DN,
  TMX_LF,
  TMX_RG,
  TMX_PW,
  TMX_NW,
  MC_PEQ,
  MC_EQEQ,
};

const uint16_t PROGMEM keymaps[][MATRIX_ROWS][MATRIX_COLS] = {

[_COL] = LAYOUT_split_3x5_3(
  KC_Q,          KC_W,          KC_F,          KC_P,          KC_G,     /**/  KC_J,     KC_L,    KC_U,            KC_Y,           KC_SCLN,
  KC_A,          KC_R,          KC_S,          KC_T,          KC_D,     /**/  KC_H,     KC_N,    KC_E,            KC_I,           KC_O,
  LCTL_T(KC_Z),  LALT_T(KC_X),  LGUI_T(KC_C),  KC_V,          KC_B,     /**/  KC_K,     KC_M,    LGUI_T(KC_COMM), LALT_T(KC_DOT), LCTL_T(KC_SLSH),
                    KC_LSPO, LT(_NAV, KC_SPACE), LT(_NUM, KC_QUOT),     /**/  LT(_NUM, KC_TAB), LT(_SYM, KC_BSPC), KC_RSPC
),
[_SYM] = LAYOUT_split_3x5_3(
  KC_BSLS,       KC_PERC,       KC_SLSH,       KC_ASTR,       KC_PLUS,  /**/  KC_QUES,  KC_AMPR, KC_PIPE,         KC_EXLM,        KC_SCLN,
  KC_AT,         KC_COLN,       KC_LCBR,       KC_RCBR,       KC_EQL,   /**/  KC_HASH,  KC_UNDS, KC_MINS,         KC_TILD,        KC_GRV,
  KC_CIRC,       KC_NO,         KC_LBRC,       KC_RBRC,       KC_MINS,  /**/  MO(_PRG), MO(_PRG),KC_RGUI,         KC_RALT,        KC_RCTL,
                                            _______, KC_ENT,  _______,  /**/  _______,  _______, _______
),
[_NAV] = LAYOUT_split_3x5_3(
  QK_BOOT,       KC_NO,         KC_NO,         KC_NO,         KC_NO,    /**/  KC_NO,   KC_HOME, KC_UP,            KC_END,         KC_NO,
  KC_LCTL,       KC_LALT,       KC_LGUI,       KC_LSFT,       KC_NO,    /**/  KC_PGUP, KC_LEFT, KC_DOWN,          KC_RIGHT,       KC_NO,
  KC_LCTL,       KC_LALT,       KC_LGUI,       MO(_VIW),      MO(_TMX), /**/  KC_PGDN, KC_NO,   KC_NO,            KC_NO,          KC_NO,
                                          _______, _______, _______,    /**/  KC_ESC,  KC_DEL,  _______
),
[_NUM] =  LAYOUT_split_3x5_3(
  KC_F9,         KC_F10,        KC_F11,        KC_F12,        KC_NO,    /**/  KC_MINS,  KC_7,   KC_8,             KC_9,           KC_SLSH,
  LCTL_T(KC_F4), LALT_T(KC_F5), LGUI_T(KC_F6), LSFT_T(KC_F8), KC_NO,    /**/  KC_PLUS,  KC_4,   KC_5,             KC_6,           KC_ASTR,
  LCTL_T(KC_F1), LALT_T(KC_F2), LGUI_T(KC_F3), KC_F4,         KC_NO,    /**/  KC_0,     KC_1,   LGUI_T(KC_2),     LALT_T(KC_3),   LCTL_T(KC_EQL),
                                          _______, _______, _______,    /**/  _______, _______, _______
),
[_VIW] =  LAYOUT_split_3x5_3(
  _______,       _______,       _______,       _______,       _______,  /**/  _______, _______, VIW_UP,           _______,        _______,
  _______,       _______,       _______,       _______,       _______,  /**/  _______, VIW_LF,  VIW_DN,           VIW_RG,         _______,
  _______,       _______,       _______,       _______,       _______,  /**/  _______, _______, _______,          _______,        _______,
                                            _______, _______, _______,  /**/  _______,  _______, _______
),
[_TMX] =  LAYOUT_split_3x5_3(
  _______,       _______,       _______,       _______,       _______,  /**/  _______, TMX_PW,  TMX_UP,           TMX_NW,         _______,
  _______,       _______,       _______,       _______,       _______,  /**/  _______, TMX_LF,  TMX_DN,           TMX_RG,         _______,
  _______,       _______,       _______,       _______,       _______,  /**/  _______, _______, _______,          _______,        _______,
                                            _______, _______, _______,  /**/  _______,  _______, _______
),
[_PRG] =  LAYOUT_split_3x5_3(
  _______,       _______,       _______,       _______,       MC_PEQ,   /**/  _______, _______, _______,          _______,        _______,
  _______,       _______,       _______,       _______,       MC_EQEQ,  /**/  _______, _______, _______,          _______,        _______,
  _______,       _______,       _______,       _______,       _______,  /**/  _______, _______, _______,          _______,        _______,
                                            _______, _______, _______,  /**/  _______,  _______, _______
)
};

// [_] =  LAYOUT_split_3x5_3(
//   _______,       _______,       _______,       _______,       _______,  /**/  _______, _______, _______,          _______,        _______,
//   _______,       _______,       _______,       _______,       _______,  /**/  _______, _______, _______,          _______,        _______,
//   _______,       _______,       _______,       _______,       _______,  /**/  _______, _______, _______,          _______,        _______,
//                                             _______, _______, _______,  /**/  _______,  _______, _______
// )

bool process_record_user(uint16_t keycode, keyrecord_t *record) {
    bool pressed = record->event.pressed;

    switch (keycode) {
		case VIW_UP:
			if (pressed) {
				SEND_STRING(SS_LCTL("w") SS_TAP(X_UP));
			}
			return false;
		case VIW_DN:
			if (pressed) {
				SEND_STRING(SS_LCTL("w") SS_TAP(X_DOWN));
			}
			return false;
		case VIW_LF:
			if (pressed) {
				SEND_STRING(SS_LCTL("w") SS_TAP(X_LEFT));
			}
			return false;
		case VIW_RG:
			if (pressed) {
				SEND_STRING(SS_LCTL("w") SS_TAP(X_RIGHT));
			}
			return false;
		case TMX_UP:
			if (pressed) {
				SEND_STRING(SS_LCTL("b") SS_TAP(X_UP));
			}
			return false;
		case TMX_DN:
			if (pressed) {
				SEND_STRING(SS_LCTL("b") SS_TAP(X_DOWN));
			}
			return false;
		case TMX_LF:
			if (pressed) {
				SEND_STRING(SS_LCTL("b") SS_TAP(X_LEFT));
			}
			return false;
		case TMX_RG:
			if (pressed) {
				SEND_STRING(SS_LCTL("b") SS_TAP(X_RIGHT));
			}
			return false;
		case TMX_PW:
			if (pressed) {
				SEND_STRING(SS_LCTL("b") SS_TAP(X_P));
			}
			return false;
		case TMX_NW:
			if (pressed) {
				SEND_STRING(SS_LCTL("b") SS_TAP(X_N));
			}
			return false;
		case MC_EQEQ:
			if (pressed) {
				SEND_STRING("==");
			}
			return false;
		case MC_PEQ:
			if (pressed) {
				SEND_STRING("+=");
			}
			return false;
        default:
            return true;
    }
}

